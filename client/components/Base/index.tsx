import type { NextComponentType, NextPageContext } from 'next'

import { ReactNode } from 'react'
import { Container, Box, useTheme } from '@chakra-ui/react'

import Navbar from '../Navbar'
import Footer from '../Footer'

interface BaseProps {
  children: ReactNode
}

const Base: NextComponentType<NextPageContext, {}, BaseProps> = ({
  children,
}) => {
  const theme = useTheme()
  return (
    // <Box bg={"black"} minH={"100vh"}></Box>
    <Box bg={theme.colors.primary.main} minH={'100vh'}>
      <Navbar />
      <Container maxW={'7xl'}>{children}</Container>
      {/* <Footer /> */}
    </Box>
  )
}

export default Base
